package com.example.imc;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {
    private EditText edtPeso, edtAltura;
    private Button btnCalculo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btnCalculo = (Button) findViewById(R.id.btnCalculo);
        edtPeso = (EditText) findViewById(R.id.edtPeso);
        edtAltura = (EditText) findViewById(R.id.edtAltura);
        ImageView  imagem = findViewById(R.id.imageView3);
        imagem.setImageResource(R.drawable.perfil);

        btnCalculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Float peso = Float.parseFloat(edtPeso.getText().toString());
                Float altura = Float.parseFloat(edtAltura.getText().toString());
                Float imc = peso/(altura*altura);

                if ( imc <= 18.5  ) {
                    Toast.makeText(getApplicationContext(), " Você está abaixo do peso " + imc, Toast.LENGTH_LONG).show();
                    ImageView  imagem1 = findViewById(R.id.imageView3);
                    imagem1.setImageResource(R.drawable.abaixopeso);
                }


                else if ( imc >= 18.5 & imc <= 24.9 ){

                    Toast.makeText(getApplicationContext(), " Você está no peso ideal " + imc, Toast.LENGTH_LONG).show();
                    ImageView  imagem2 = findViewById(R.id.imageView3);
                    imagem2.setImageResource(R.drawable.normal);
                }

                else if ( imc >= 25 & imc <= 29.9 ){

                    Toast.makeText(getApplicationContext(), " Você está levemente acima do peso " + imc, Toast.LENGTH_LONG).show();
                    ImageView  imagem3 = findViewById(R.id.imageView3);
                    imagem3.setImageResource(R.drawable.sobrepeso);
                }

                else if ( imc >= 30 & imc <= 34.9 ){

                    Toast.makeText(getApplicationContext(), " Você está com obesidade grau I "+ imc, Toast.LENGTH_LONG).show();
                    ImageView  imagem4 = findViewById(R.id.imageView3);
                    imagem4.setImageResource(R.drawable.obesidade1);
                }

                else if ( imc >= 35 & imc <= 39.9 ){

                    Toast.makeText(getApplicationContext(), " Você está com obesidade grau II (severa) "+ imc, Toast.LENGTH_LONG).show();
                    ImageView  imagem5 = findViewById(R.id.imageView3);
                    imagem5.setImageResource(R.drawable.obesidade2);
                }

                else if ( imc >= 40 ){

                    Toast.makeText(getApplicationContext(), " Você está com obesidade grau III (mórbida) " + imc, Toast.LENGTH_LONG).show();
                    ImageView  imagem6 = findViewById(R.id.imageView3);
                    imagem6.setImageResource(R.drawable.obesidade3);
                }
            }

        });

    }


}
